#include <vector>
#include <iostream>
#include <par_classifier.hh>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    using namespace classifier::par;
    using namespace cv;
    using Vector = Eigen::Vector3i;

    if (argc != 3)
    {
        std::cout << argv[0] << " picture k" << std::endl;
        return 1;
    }

    // Load image
    Mat image = imread(argv[1], CV_LOAD_IMAGE_COLOR);

    if (!image.data) // Check for invalid input
    {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    namedWindow("window", CV_WINDOW_AUTOSIZE); // Create a window for display.
    imshow("window", image);                   // Show our image inside it.
    waitKey();


    // Create the distance function
    auto distance = [](const Vector& v1,
                       const Vector& v2) -> double
    {
        return (v1 - v2).squaredNorm();
    };

    std::cout << "Creating options" << std::endl;
    KMeansOptions options{(uint32_t)atoi(argv[2]), 3, (uint32_t)(image.rows * image.cols)};

    // Create vectors from image
    std::cout << "Creating vector" << std::endl;
    std::vector<Vector> data;
    data.reserve(options.nbVectors);

    std::cout << "Filling vector" << std::endl;
    for(int i = 0; i < image.rows; ++i)
        for(int j = 0; j < image.cols; ++j)
            data.emplace_back(
                    image.at<Vec3b>(i, j)[0],
                    image.at<Vec3b>(i, j)[1],
                    image.at<Vec3b>(i, j)[2]);
            //data.emplace_back(image.at<uint8_t>(i,j), image.at<uint8_t>(i,j), image.at<uint8_t>(i,j));

    std::cout << "Init tbb" << std::endl;
    tbb::task_scheduler_init init(8);

    std::cout << "Run KMeans" << std::endl;
    std::cout << "Clustering: " << data.size() << " 3d vectors" << std::endl;
    auto res = par_kmeans(options, distance, data);

    std::cout << "Generating output picture" << std::endl;
    Mat output = Mat::zeros(image.rows, image.cols, CV_8UC3);
    for (unsigned i = 0; i < data.size(); ++i)
    {
        unsigned x = i % image.cols;
        unsigned y = i / image.cols;
        output.at<cv::Vec3b>(y, x)[0] = 255 / (res[i] + 1);
        output.at<cv::Vec3b>(y, x)[1] = 255 / (res[i] + 1);
        output.at<cv::Vec3b>(y, x)[2] = 255 / (res[i] + 1);
    }

    imshow("window", output);                   // Show our image inside it.
    waitKey();

    return 0;
}

