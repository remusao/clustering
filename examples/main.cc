#include <vector>
#include <iostream>
#include <classifier.hh>
#include <par_classifier.hh>
#include "tbb/task_scheduler_init.h"
#include "tbb/tick_count.h"



#define K 15
#define DIM 4
#define NBVECTORS 5000000

int main()
{
    using namespace classifier;
    // Init random data
    std::vector<Eigen::Vector4d> data;
    data.reserve(NBVECTORS);

    par::KMeansOptions poptions{K, DIM, NBVECTORS};
    KMeansOptions      options{K, DIM, NBVECTORS};

    // Create the distance function
    auto distance = [](const Eigen::Vector4d& v1,
                       const Eigen::Vector4d& v2) -> double
    {
        return (v1 - v2).squaredNorm();
    };

    for (unsigned i = 0; i < NBVECTORS; ++i)
        data.emplace_back(Eigen::Vector4d::Random());


    auto t0 = tbb::tick_count::now();
    auto res = kmeans(options, distance, data);
    auto t1 = tbb::tick_count::now();

    double ref = (t1-t0).seconds() * 1000;

    for (unsigned i = 2; i < 10; ++i)
    {
        tbb::task_scheduler_init init(i);

        auto t0 = tbb::tick_count::now();
        auto res = par::par_kmeans(poptions, distance, data);
        auto t1 = tbb::tick_count::now();
        double t = (t1-t0).seconds() * 1000;
        std::cout << i << " threads: " << t << " msecs: " << ref / t << " speedup" << std::endl;
    }
    return 0;
}

