#include <vector>
#include <iostream>
#include <classifier.hh>

#define K 15
#define DIM 4
#define NBVECTORS 5000000

int main()
{
    using namespace classifier;
    // Init random data
    Eigen::Matrix4Xd data(DIM, NBVECTORS);
    data.setRandom();

    KMeansOptions options{K, DIM, NBVECTORS};

    // Create the distance function
    auto distance = [](const Eigen::Vector4d& v1,
                       const Eigen::Vector4d& v2) -> double
    {
        return (v1 - v2).squaredNorm();
    };

    auto res = kmeans(options, distance, data);

    return 0;
}

