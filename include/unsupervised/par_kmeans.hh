#ifndef PAR_KMEANS_HH_
# define PAR_KMEANS_HH_

# include <vector>
# include <Eigen/Dense>
# include "tbb/tbb.h"

namespace classifier
{
    namespace par
    {
        struct KMeansOptions
        {
            uint32_t k;
            uint32_t dim;
            uint32_t nbVectors;
        };

        template <typename Vector, typename Distance>
        std::vector<uint32_t>
        par_kmeans(KMeansOptions options,
                   const Distance& dist,
                   const std::vector<Vector>& data);

// Implementation
# include "par_kmeans.hxx"
    }
}


#endif /* !PAR_KMEANS_HH_ */
