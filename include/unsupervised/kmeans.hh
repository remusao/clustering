#ifndef KMEANS_HH_
# define KMEANS_HH_

# include <vector>
# include <Eigen/Dense>

namespace classifier
{
    struct KMeansOptions
    {
        uint32_t k;
        uint32_t dim;
        uint32_t nbVectors;
    };


    // KMeans algorithm
    template <typename Vector, typename Distance>
    std::vector<uint32_t>
    kmeans(KMeansOptions options,
           const Distance& dist,
           const std::vector<Vector>& data);

// Implementation
# include "kmeans.hxx"

}


#endif /* !KMEANS_HH_ */
