
namespace
{
    template <typename Vector, typename Distance>
    bool par_cluster(
        const KMeansOptions& options,
        const Distance& distance,
        std::vector<uint32_t>& clusters,
        const std::vector<Vector>& centroids,
        const std::vector<Vector>& data)
    {
        bool hasChanged = false;
        auto findCentroid = [&](const tbb::blocked_range<size_t>& r)
        {
            for(size_t i = r.begin(); i != r.end(); ++i)
            {
                Vector min = centroids[0];
                double d, tmp, index;
                d = distance(min, data[i]);
                index = 0;
                for (uint64_t j = 1; j < options.k; ++j)
                {
                    tmp = distance(data[i], centroids[j]);
                    if (tmp < d)
                    {
                        min = centroids[j];
                        d = tmp;
                        index = j;
                    }
                }

                if (clusters[i] != index)
                    hasChanged = true;
                clusters[i] = index;
            }
        };

        tbb::parallel_for(tbb::blocked_range<std::size_t>(0, options.nbVectors), findCentroid);

        return hasChanged;
    }


    template <typename Vector>
    void getCentroids(
        const KMeansOptions& options,
        const std::vector<unsigned>& clusters,
        const std::vector<Vector>& datas,
        std::vector<Vector>& centroids)
    {
        // Used to count the size of each cluster
        std::vector<uint64_t> sizes(options.k, 0);

        // Clear centroids before computing the mean
        for (uint32_t i = 0; i < options.k; ++i)
            centroids[i] = Vector::Zero(options.dim);

        // Sum vectors of each centroid
        for (uint64_t i = 0; i < options.nbVectors; ++i)
        {
            centroids[clusters[i]] += datas[i];
            ++sizes[clusters[i]];
        }

        // Divide by the number of element
        for (uint32_t i = 0; i < options.k; ++i)
        {
            if (sizes[i] != 0)
                centroids[i] /= (double)sizes[i];
        }

    }
}


// KMeans clustering algorithm
template <typename Vector, typename Distance>
std::vector<uint32_t>
par_kmeans(KMeansOptions options,
    const Distance& dist,
    const std::vector<Vector>& data)
{
    std::vector<uint32_t>           clusters(options.nbVectors, 0);
    std::vector<Vector>    centroids;
    centroids.reserve(options.k);

    // init random centroids
    for (unsigned i = 0; i < options.k; ++i)
        centroids.emplace_back(Vector::Random(options.dim));

    // Main loop
    // While clustering is different, compute the centroids and
    // assign each vector to one of them
    while (par_cluster(options, dist, clusters, centroids, data))
        getCentroids(options, clusters, data, centroids);

    return clusters;
}

